package com.jhakkasrecipes;

import com.google.gson.annotations.SerializedName;
import com.jhakkasrecipes.network.model.SingleRecipeModel;

import java.util.List;

public class RecipeCategoryModel {
    @SerializedName("categoryid")
    private int categoryid;

    @SerializedName("categoryname")
    private String categoryname;

    @SerializedName("recipes")
    private List<SingleRecipeModel> recipesList;

    private int getCategoryid(int categoryid){
        return this.categoryid;
    }

    private String getCategoryname(String categoryname){
        return this.categoryname;
    }

    private List<SingleRecipeModel> getRecipesList(){
        return this.recipesList;
    }
    private void setCategoryid(int categoryid){
        this.categoryid=categoryid;
    }

    private void setCategoryname(String categoryname){
        this.categoryname=categoryname;
    }

    private void setRecipesList(List<SingleRecipeModel> singleRecipeModels){
        this.recipesList=singleRecipeModels;
    }
}
