package com.jhakkasrecipes.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryRecipeModel {
    @SerializedName("categoryid")
    private int categoryid;

    @SerializedName("categoryname")
    private String categoryname;

    @SerializedName("recipes")
    private List<SingleRecipeModel> recipeModelList;

    public List<SingleRecipeModel> getRecipeModelList() {
        return this.recipeModelList;
    }

    public int getCategoryid(){
        return this.categoryid;
    }

    public String getCategoryname(){
        return this.categoryname;
    }

    public void setCategoryid(int categoryid){
        this.categoryid=categoryid;
    }

    public void setCategoryname(String categoryname){
        this.categoryname=categoryname;
    }
}
