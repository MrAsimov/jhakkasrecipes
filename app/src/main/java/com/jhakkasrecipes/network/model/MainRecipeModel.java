package com.jhakkasrecipes.network.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class MainRecipeModel {
    @SerializedName("recipecategorylist")
    private List<CategoryRecipeModel> categoryRecipeModelList;

    public List<CategoryRecipeModel> getCategoryRecipeModelList() {
        return categoryRecipeModelList;
    }

    public void setCategoryRecipeModelList(List<CategoryRecipeModel> categoryRecipeModelList){
        this.categoryRecipeModelList=categoryRecipeModelList;
    }
}
