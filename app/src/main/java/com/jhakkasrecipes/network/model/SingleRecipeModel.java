package com.jhakkasrecipes.network.model;

import com.google.gson.annotations.SerializedName;

public class SingleRecipeModel {
    @SerializedName("recipeid")
    private int recipeid;

    @SerializedName("recipetitle")
    private String recipetitle;

    @SerializedName("mainpic")
    private String mainpic;

    @SerializedName("recipeurl")
    private String recipeurl;

    public int getRecipeid(){
        return this.recipeid;
    }

    public String getRecipetitle(){
        return this.recipetitle;
    }

    public String getMainpic(){
        return this.mainpic;
    }
    public String getRecipeurl(){
        return this.recipeurl;
    }

    private void setRecipeid(int recipeid){
        this.recipeid=recipeid;
    }

    private void setRecipetitle(String recipetitle){
        this.recipetitle=recipetitle;
    }

    private void setMainpic(String mainpic){
        this.mainpic=mainpic;
    }

    private void setRecipeurl(String recipeurl){
        this.recipeurl=recipeurl;
    }
}
