package com.jhakkasrecipes.network;
import com.jhakkasrecipes.network.model.CategoryRecipeModel;
import com.jhakkasrecipes.network.model.MainRecipeModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by user on 2/8/16.
 */
public interface RECIPEApiInterface {
    @GET("/ritesh/recipes/{filename}.json")
    Call<CategoryRecipeModel> getjson(@Path("filename") int file);

    @GET("/ritesh/recipes/recipecategorylist.json")
    Call<MainRecipeModel> getRecipeCategoryList();
//    @FormUrlEncoded
//    @POST("/api/v1/user/login")
//    Call<UserLoginResponse> userLogin(@FieldMap Map<String, String> userDetail);
//

}
