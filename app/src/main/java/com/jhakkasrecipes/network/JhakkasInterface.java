package com.jhakkasrecipes.network;

import com.jhakkasrecipes.SingleCategoryModel;

import java.util.List;
import retrofit2.http.GET;
import retrofit2.Call;
import retrofit2.http.Path;

public interface JhakkasInterface {
    @GET("/apis/{id}/listofrecipes")
    Call<List<SingleCategoryModel>> getRecipes(@Path("id") String id);
}
