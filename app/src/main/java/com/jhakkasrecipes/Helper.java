package com.jhakkasrecipes;

import android.app.Activity;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.File;

public class Helper {
    public static Retrofit APISetup(Activity activity){
        final int CACHE_SIZE=10*1024*1024;
        final String CACHE_DIR="httpCache";
        OkHttpClient.Builder mOkBuilder;
        Cache mCache;
        HttpLoggingInterceptor mLogInterceptor;
        Retrofit mRetrofit;
        try{
            mOkBuilder = new OkHttpClient.Builder();
            mCache = new Cache(new File(activity.getCacheDir(),CACHE_DIR),CACHE_SIZE);
            mOkBuilder.cache(mCache);
            mLogInterceptor = new HttpLoggingInterceptor();
            mLogInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            mOkBuilder.addInterceptor(mLogInterceptor);
            mRetrofit =new Retrofit.Builder()
                    .baseUrl(Config.ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(mOkBuilder.build())
                    .build();
            return mRetrofit;
        }catch(Exception e){
            return null;
        }
    }
}
