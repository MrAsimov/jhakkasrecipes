package com.jhakkasrecipes;
import com.google.gson.annotations.SerializedName;

public class SingleCategoryModel {
    @SerializedName("id")
    private String categoryid;

    @SerializedName("category")
    private String category;

    public String getCategoryid(){
        return this.categoryid;
    }

    public String getCategory(){
        return this.category;
    }

    public SingleCategoryModel(String id,String category){
        this.categoryid=id;
        this.category=category;
    }
}
