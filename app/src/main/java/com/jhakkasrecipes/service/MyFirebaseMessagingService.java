package com.jhakkasrecipes.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
//import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jhakkasrecipes.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by user on 16/12/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage != null && remoteMessage.getData() != null && !remoteMessage.getData().isEmpty()) {
            Log.d("onMessageReceived", "1");
            Log.d("onMessageReceived", "1" + remoteMessage.getData().get("body"));
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Bitmap remote_picture = null;
            Bitmap remote_icon = null;

            // Create the style object with BigPictureStyle subclass.
            android.support.v4.app.NotificationCompat.BigPictureStyle notiStyle = new android.support.v4.app.NotificationCompat.BigPictureStyle();

            try {
                if(remoteMessage.getData().get("imageurl")!=null && !remoteMessage.getData().get("imageurl").isEmpty() && remoteMessage.getData().get("imageurl").length()>0) {
                    System.out.println("imageurl="+remoteMessage.getData().get("imageurl"));
                    remote_picture = BitmapFactory.decodeStream((InputStream) new URL(remoteMessage.getData().get("imageurl")).getContent());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            try{
                if(remoteMessage.getData().get("iconurl")!=null && !remoteMessage.getData().get("iconurl").isEmpty() && remoteMessage.getData().get("iconurl").length()>0) {
                    System.out.println("iconurl="+remoteMessage.getData().get("iconurl"));
//                    String checkurl="http://professorio.com/graphic-stories/library/professorio-where-is-happiness.jpg";
                    remote_icon = BitmapFactory.decodeStream((InputStream) new URL(remoteMessage.getData().get("iconurl")).getContent());
//                    remote_icon = BitmapFactory.decodeStream((InputStream) new URL(checkurl).getContent());

                }
            }catch(IOException e){
                e.printStackTrace();
            }

            // Add the big picture to the style.
            notiStyle.bigPicture(remote_picture);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            Intent resultIntent;

            // Creates an explicit intent for an ResultActivity to receive.
            if (remoteMessage.getData().get("click_action") != null && !remoteMessage.getData().get("click_action").isEmpty() && remoteMessage.getData().get("click_action").length() > 0) {

                if (remoteMessage.getData().get("click_action").equalsIgnoreCase("android.intent.action.launch_story_detail_activity")) {
//                    resultIntent = new Intent(this, StoryDetailActivity.class);
//                    if (remoteMessage.getData().get("id") != null && !remoteMessage.getData().get("id").isEmpty() && remoteMessage.getData().get("id").length() > 0) {
//                        resultIntent.putExtra("storyId", remoteMessage.getData().get("id"));
//                    }
//                    resultIntent.putExtra("lastactivity", "notification");
//                    stackBuilder.addParentStack(StoryDetailActivity.class);
//                    stackBuilder.addNextIntent(resultIntent);
                } else if (remoteMessage.getData().get("click_action").equalsIgnoreCase("android.intent.action.launch_product_activity")) {
//                    resultIntent = new Intent(this, ShopProductDetailsActivity.class);
//                    if (remoteMessage.getData().get("id") != null && !remoteMessage.getData().get("id").isEmpty() && remoteMessage.getData().get("id").length() > 0) {
//                        resultIntent.putExtra("productid", remoteMessage.getData().get("id"));
//                    }
//                    resultIntent.putExtra("lastactivity", "notification");
//                    stackBuilder.addParentStack(ShopProductDetailsActivity.class);
//                    stackBuilder.addNextIntent(resultIntent);
                } else if (remoteMessage.getData().get("click_action").equalsIgnoreCase("android.intent.action.launch_product_list_activity")) {
//                    resultIntent = new Intent(this, ShopProductListGridViewActivity.class);
//                    if (remoteMessage.getData().get("id") != null && !remoteMessage.getData().get("id").isEmpty() && remoteMessage.getData().get("id").length() > 0) {
//                        resultIntent.putExtra("collectionid", remoteMessage.getData().get("id"));
//                    }
//                    resultIntent.putExtra("categoryName", remoteMessage.getData().get("categoryName"));
//                    resultIntent.putExtra("lastactivity", "notification");
//                    stackBuilder.addParentStack(ShopProductListGridViewActivity.class);
//                    stackBuilder.addNextIntent(resultIntent);
                }
            }else{
//                resultIntent = new Intent(this, BLZMActivity.class);
//                stackBuilder.addParentStack(BLZMActivity.class);
//                stackBuilder.addNextIntent(resultIntent);
            }

            // Adds the Intent that starts the Activity to the top of the stack.

            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            Notification noti;
            String title="";
            String body="";
            if(remoteMessage.getData().get("title")!=null && !remoteMessage.getData().get("title").isEmpty()) {
                title = remoteMessage.getData().get("title");
            }
            if(remoteMessage.getData().get("body")!=null && !remoteMessage.getData().get("body").isEmpty()) {
                body = remoteMessage.getData().get("body");
            }
            //Extra 30th december
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
//            //notificationBuilder.setSmallIcon(R.mipmap.icon);//used for joybynature
//            notificationBuilder.setSmallIcon(R.drawable.blozzomicon);
//            notificationBuilder.setAutoCancel(true);
//            if(remote_icon!=null) {
//                notificationBuilder.setLargeIcon(remote_icon);
//            }
//            notificationBuilder.setContentIntent(resultPendingIntent);
//            notificationBuilder.setContentTitle(title);
//            notificationBuilder.setContentText(body);
//            notificationBuilder.setColor(Color.parseColor("#ff0000"));//addedd on nov11
//
//            if(remote_picture!=null) {
//                notificationBuilder.setStyle(notiStyle);
//            }
//            noti=notificationBuilder.build();
            //Extra 30th december Ends

//            noti.defaults |= Notification.DEFAULT_LIGHTS;
//            noti.defaults |= Notification.DEFAULT_VIBRATE;
//            noti.defaults |= Notification.DEFAULT_SOUND;
//
//            noti.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
//            int requestID1 = (int) System.currentTimeMillis();
//            //mNotificationManager.notify(0, noti);//original
//            mNotificationManager.notify(requestID1, noti);//ADDED on 3rd january
        }
        Log.d("onMessageReceived", "complete");
    }
}
