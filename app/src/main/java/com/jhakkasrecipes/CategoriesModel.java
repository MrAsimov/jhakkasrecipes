package com.jhakkasrecipes;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CategoriesModel {
    @SerializedName("categories")
    List<SingleCategoryModel> categories;

    public List<SingleCategoryModel> getCategories() {
        return categories;
    }
}
