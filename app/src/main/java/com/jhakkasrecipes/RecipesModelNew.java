package com.jhakkasrecipes;

import com.google.gson.annotations.SerializedName;

public class RecipesModelNew {
    @SerializedName("id")
    private String categoryid;

    @SerializedName("recipeid")
    private String recipeid;

    @SerializedName("recipetitle")
    private String recipeTitle;

    @SerializedName("mainpic")
    private String mainpicurl;

    @SerializedName("recipeurl")
    private String recipeurl;


    public void setRecipeurl(String url){
        this.recipeurl=url;
    }
    public String getRecipeurl(){
        return this.recipeurl;
    }
    public String getCategoryid(){
        return this.categoryid;
    }

    public String getRecipeid(){
        return this.recipeid;
    }

    public String getRecipeTitle(){
        return this.recipeTitle;
    }

    public String getMainpicurl(){
        return this.mainpicurl;
    }

    public void setCategoryid(String id){
        this.categoryid=id;
    }

    public void setRecipeid(String id){
        this.recipeid=id;
    }

    public void setRecipeTitle(String title){
        this.recipeTitle=title;
    }

    public void setMainpicurl(String mainpicurl){
        this.mainpicurl=mainpicurl;
    }
}
