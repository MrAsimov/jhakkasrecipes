package com.jhakkasrecipes.activity;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayerView;
import com.jhakkasrecipes.R;
import com.jhakkasrecipes.RecipesModelNew;
import com.jhakkasrecipes.Share;

import java.util.ArrayList;
import java.util.List;

public class RecipeDetailActivity extends AppCompatActivity {
    RecipesModelNew recipesShortModel=new RecipesModelNew();

    private TextView detail;
    private YouTubePlayerView youTubeView;
    private ImageView back;

    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private LinearLayout mainlayout;
    private ImageView share;
    @Override
    protected void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.recipe_detail_activity_new);
        mainlayout=(LinearLayout)findViewById(R.id.main_linear);
        String url=getIntent().getStringExtra("recipeurl");
        url.trim();
        setNewData();
        WebView webView=(WebView)findViewById(R.id.webview);
        share = (ImageView)findViewById(R.id.story_detail_share_icon);
        back =(ImageView)findViewById(R.id.back_image);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        //webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.loadUrl(url);
        System.out.println("fixedurl="+recipesShortModel.getRecipeurl());
        System.out.println("intenturl="+url);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareArticle();

            }
        });

    }
    private void ShareArticle(){
        Share shareRa=new Share(this);
        shareRa.shareProduct(recipesShortModel.getRecipeurl());
    }
    private void setNewData(){

        recipesShortModel.setCategoryid("1");
        recipesShortModel.setRecipeid("1");
        recipesShortModel.setRecipeTitle("title");
        recipesShortModel.setMainpicurl("http://professorio.com/inspirationalposters/inspiposters/do-it-with-passion-or-not-at-all.jpg");
        recipesShortModel.setRecipeurl("https://hebbarskitchen.com/schezwan-fried-rice-recipe/");
    }
//    @Override
//    public void onInitializationFailure(YouTubePlayer.Provider provider,
//                                        YouTubeInitializationResult errorReason) {
//        if (errorReason.isUserRecoverableError()) {
//            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
//        } else {
//            String errorMessage = String.format(
//                    getString(R.string.error_player), errorReason.toString());
//            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
//        }
//
//    }

//
//    @Override
//    public void onInitializationSuccess(YouTubePlayer.Provider provider,
//                                        YouTubePlayer player, boolean wasRestored) {
//        if (!wasRestored) {
//
//            // loadVideo() will auto play video
//            // Use cueVideo() method, if you don't want to play it automatically
//            //player.loadVideo(Config.YOUTUBE_VIDEO_CODE);
//            player.cueVideo(Config.YOUTUBE_VIDEO_CODE);
//
//            // Hiding player controls
//            player.setPlayerStyle(PlayerStyle.CHROMELESS);
//        }
//
//    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == RECOVERY_DIALOG_REQUEST) {
//            // Retry initialization if user performed a recovery action
//            getYouTubePlayerProvider().initialize(Config.DEVELOPER_KEY, this);
//        }
//    }
//
//    private YouTubePlayer.Provider getYouTubePlayerProvider() {
//        return (YouTubePlayerView) findViewById(R.id.youtube_view);
//
//    }
}
