package com.jhakkasrecipes.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.jhakkasrecipes.Helper;
import com.jhakkasrecipes.R;
import com.jhakkasrecipes.RecipeListAdapter;
import com.jhakkasrecipes.network.RECIPEApiInterface;
import com.jhakkasrecipes.network.model.CategoryRecipeModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteRecipeActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecipeListAdapter recipeListAdapter;
    protected void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_favourite);
        recyclerView=(RecyclerView) findViewById(R.id.recycler_view_list_recipe);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));

        newDataService("1");
    }

    public void newDataService(String categoryid){
        System.out.println("newdataservice");
        RECIPEApiInterface recipeApiInterface= Helper.APISetup(this).create(RECIPEApiInterface.class);
        Call<CategoryRecipeModel> call=recipeApiInterface.getjson(Integer.parseInt(categoryid));
        call.enqueue(new Callback<CategoryRecipeModel>() {
            @Override
            public void onResponse(Call<CategoryRecipeModel> call, Response<CategoryRecipeModel> response) {
                System.out.println("success");
                adapter(response);
            }

            @Override
            public void onFailure(Call<CategoryRecipeModel> call, Throwable t) {
                System.out.println("onfailurethrowable");
            }
        });
    }
    public void adapter(Response<CategoryRecipeModel> response){
        recipeListAdapter = new RecipeListAdapter(this,response.body().getRecipeModelList());
        recyclerView.setAdapter(recipeListAdapter);
    }
}
