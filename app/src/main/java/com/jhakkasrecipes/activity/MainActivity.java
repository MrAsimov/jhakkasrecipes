package com.jhakkasrecipes.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.jhakkasrecipes.Helper;
import com.jhakkasrecipes.ListOfRecipesForaCategoryFragment;
import com.jhakkasrecipes.R;
import com.jhakkasrecipes.SingleCategoryModel;
import com.jhakkasrecipes.ViewPagerAdapter;
import com.jhakkasrecipes.network.RECIPEApiInterface;
import com.jhakkasrecipes.network.model.CategoryRecipeModel;
import com.jhakkasrecipes.network.model.MainRecipeModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<SingleCategoryModel> categoriesTest=new ArrayList<SingleCategoryModel>();
    private DrawerLayout mDrawerLayout;
    private List<CategoryRecipeModel> categoryRecipeModelList=new ArrayList<CategoryRecipeModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
     //   setLocalData();//will be commented
        setSupportActionBar(toolbar);
//        initNavigationDrawer();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.drawable_icon);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
//        setupViewPager(viewPager);
//        tabLayout = (TabLayout) findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(viewPager);
        recipeCategoryList();
        //recipeDataService(1);

    }

    private void setupViewPagerNew(){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for(int i=0;i<categoryRecipeModelList.size();i++){
            ListOfRecipesForaCategoryFragment fragment=new ListOfRecipesForaCategoryFragment();
            fragment.setdata(String.valueOf(categoryRecipeModelList.get(i).getCategoryid()),categoryRecipeModelList.get(i).getCategoryname());
            adapter.addFragment(fragment, categoryRecipeModelList.get(i).getCategoryname());
        }
        viewPager.setAdapter(adapter);
    }
//    private void setupViewPager(ViewPager viewPager) {
//        //In manvigor we are using viewtabadapter
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        for(int i=0;i<categoriesTest.size();i++){
//            ListOfRecipesForaCategoryFragment fragment=new ListOfRecipesForaCategoryFragment();
//            fragment.setdata(categoriesTest.get(i).getCategoryid(),categoriesTest.get(i).getCategory());
//            adapter.addFragment(fragment, categoriesTest.get(i).getCategory());
//        }
//        viewPager.setAdapter(adapter);
//    }
    //will be commented
//    private void setLocalData(){
//        SingleCategoryModel model1=new SingleCategoryModel("1","Home");
//        SingleCategoryModel model2=new SingleCategoryModel("2","BREAKFAST RECIPES");
//        SingleCategoryModel model3=new SingleCategoryModel("3","PANEER RECIPES");
//        SingleCategoryModel model4=new SingleCategoryModel("4","CURRY RECIPES");
//        SingleCategoryModel model5=new SingleCategoryModel("5","SNACK RECIPES");
//
//        categoriesTest.add(model1);
//        categoriesTest.add(model2);
//        categoriesTest.add(model3);
//        categoriesTest.add(model4);
//        categoriesTest.add(model5);
//
//    }
    public void initNavigationDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        final Menu menu = navigationView.getMenu();
        for(int i=0;i<categoryRecipeModelList.size();i++){
            //menu.add(categoriesTest.get(i).getCategory());

            menu.add(i,i+1,i+2,categoryRecipeModelList.get(i).getCategoryname());
        }
        menu.add(categoryRecipeModelList.size(),categoryRecipeModelList.size()+1,categoryRecipeModelList.size()+2,"Favourite");

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getGroupId()<categoryRecipeModelList.size()) {
                    System.out.println("itemordergroupid=" + item.getGroupId());
                    System.out.println("itemorder=" + item.getOrder());
                    System.out.println("itemordergetitemid=" + item.getItemId());
                    viewPager.setCurrentItem(item.getGroupId());
                    mDrawerLayout.closeDrawers();
                }else if(item.getGroupId()==categoryRecipeModelList.size()){
                    startFavActivity();
                }

                //System.out.println("itemordergetitemid="+item);
                return false;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.drawable_icon);
    }
    private void startFavActivity(){
        Intent intent=new Intent(this,FavouriteRecipeActivity.class);
        startActivity(intent);
    }
//    private void recipeDataService(int file){
//        RECIPEApiInterface recipeApiInterface= Helper.APISetup(this).create(RECIPEApiInterface.class);
//        Call<CategoryRecipeModel> call = recipeApiInterface.getjson(file);
//        call.enqueue(new Callback<CategoryRecipeModel>() {
//            @Override
//            public void onResponse(Call<CategoryRecipeModel> call, Response<CategoryRecipeModel> response) {
//                System.out.println("onresponsera");
//            }
//
//            @Override
//            public void onFailure(Call<CategoryRecipeModel> call, Throwable t) {
//                System.out.println("failure");
//            }
//        });
//    }

    private void recipeCategoryList(){
        RECIPEApiInterface recipeApiInterface= Helper.APISetup(this).create(RECIPEApiInterface.class);
        Call<MainRecipeModel> call = recipeApiInterface.getRecipeCategoryList();
        call.enqueue(new Callback<MainRecipeModel>() {
            @Override
            public void onResponse(Call<MainRecipeModel> call, Response<MainRecipeModel> response) {
                if(response!=null && response.body()!=null && response.body().getCategoryRecipeModelList()!=null){
                    categoryRecipeModelList=response.body().getCategoryRecipeModelList();
                    System.out.println("recipeCategoryList onresponse");
                    //setData(response.body().getCategoryRecipeModelList());
                    setupViewPagerNew();
                    tabLayout = (TabLayout) findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(viewPager);
                    initNavigationDrawer();
                }
            }

            @Override
            public void onFailure(Call<MainRecipeModel> call, Throwable t) {

            }
        });
    }
}
