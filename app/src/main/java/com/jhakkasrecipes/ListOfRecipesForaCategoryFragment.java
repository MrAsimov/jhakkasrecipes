package com.jhakkasrecipes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.jhakkasrecipes.network.RECIPEApiInterface;
import com.jhakkasrecipes.network.model.CategoryRecipeModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListOfRecipesForaCategoryFragment extends Fragment {
    private String categoryid,category;
    private View view;
    private  RecyclerView recyclerView;
    private RecipeListAdapter recipeListAdapter;
    public ListOfRecipesForaCategoryFragment() {
        // Required empty public constructor
    }

    public void setdata(String id,String category){
        this.categoryid=id;
        this.category=category;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("printingra"+categoryid+" "+category);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_one, container, false);
        recyclerView=(RecyclerView) view.findViewById(R.id.recycler_view_list_recipe);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        newDataService(this.categoryid);
        return view;
    }

    public void newDataService(String categoryid){

        RECIPEApiInterface recipeApiInterface=Helper.APISetup(getActivity()).create(RECIPEApiInterface.class);
        Call<CategoryRecipeModel> call=recipeApiInterface.getjson(Integer.parseInt(categoryid));
        call.enqueue(new Callback<CategoryRecipeModel>() {
            @Override
            public void onResponse(Call<CategoryRecipeModel> call, Response<CategoryRecipeModel> response) {
                recipeListAdapter = new RecipeListAdapter(getActivity(),response.body().getRecipeModelList());
                recyclerView.setAdapter(recipeListAdapter);
            }

            @Override
            public void onFailure(Call<CategoryRecipeModel> call, Throwable t) {

            }
        });
    }
}
