package com.jhakkasrecipes;

import com.google.gson.annotations.SerializedName;

public class RecipesShortModel {
    @SerializedName("id")
    String categoryid;

    @SerializedName("recipeid")
    String recipeid;

    @SerializedName("recipetitle")
    private String recipeTitle;

    @SerializedName("mainpic")
    String mainpicurl;

    public String getCategoryid(){
        return this.categoryid;
    }

    public String getRecipeid(){
        return this.recipeid;
    }

    public String getRecipeTitle(){
        return this.recipeTitle;
    }

    public String getMainpicurl(){
        return this.mainpicurl;
    }

    public void setCategoryid(String id){
        this.categoryid=id;
    }

    public void setRecipeid(String id){
        this.recipeid=id;
    }

    public void setRecipeTitle(String title){
        this.recipeTitle=title;
    }

    public void setMainpicurl(String mainpicurl){
        this.mainpicurl=mainpicurl;
    }
}
