package com.jhakkasrecipes;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.jhakkasrecipes.activity.RecipeDetailActivity;
import com.jhakkasrecipes.network.model.MainRecipeModel;
import com.jhakkasrecipes.network.model.SingleRecipeModel;

import java.util.List;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.SingleViewModel>{
    private List<RecipesShortModel> recipesShortModelList;
    private Activity mActivity;
    private List<SingleCategoryModel> singleCategoryModelsList;
    private MainRecipeModel mainRecipeModelsList;
    private List<SingleRecipeModel> singleRecipeModelList;

    public class SingleViewModel extends RecyclerView.ViewHolder{
        private TextView RecipeTitle,TagPreference;
        private SimpleDraweeView mainimage;
        public SingleViewModel(View view){
            super(view);
            RecipeTitle=(TextView)view.findViewById(R.id.recipe_title);
            mainimage=(SimpleDraweeView)view.findViewById(R.id.recipe_image_drawee);
            RecipeTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getAdapterPosition();
                    startActivity(getAdapterPosition());
                }
            });
        }
    }
    private void startActivity(int position){
        Intent intent=new Intent(this.mActivity.getApplicationContext(),RecipeDetailActivity.class);
        intent.putExtra("recipeurl",this.singleRecipeModelList.get(position).getRecipeurl());
        mActivity.startActivity(intent);
    }
//    public RecipeListAdapter(Activity activity,List<RecipesShortModel> recipesShortModelList){
//        this.recipesShortModelList=recipesShortModelList;
//        this.mActivity=activity;
//    }
//    public RecipeListAdapter(Activity activity,List<SingleCategoryModel> singleCategoryModelsList){
//        this.singleCategoryModelsList=singleCategoryModelsList;
//        this.mActivity=activity;
//    }

//    public RecipeListAdapter(Activity activity,MainRecipeModel singleCategoryModelsList){
//        //this.singleCategoryModelsList=singleCategoryModelsList;
//        this.mainRecipeModelsList=singleCategoryModelsList;
//        this.mActivity=activity;
//    }

    public RecipeListAdapter(Activity activity,List<SingleRecipeModel> singleCategoryModelsList){
        //this.singleCategoryModelsList=singleCategoryModelsList;
        this.singleRecipeModelList=singleCategoryModelsList;
        this.mActivity=activity;
    }
    public int getItemCount(){
        return this.singleRecipeModelList.size();
        //return this.recipesShortModelList.size();
    }
    public void onBindViewHolder(SingleViewModel holder, final int position){
//        holder.RecipeTitle.setText(this.recipesShortModelList.get(position).getRecipeTitle());
//        Uri uri= Uri.parse(recipesShortModelList.get(position).getMainpicurl());
//        System.out.println("urlink="+recipesShortModelList.get(position).getMainpicurl());
//        holder.mainimage.setImageURI(uri);

        holder.RecipeTitle.setText(this.singleRecipeModelList.get(position).getRecipetitle());
        Uri uri= Uri.parse(this.singleRecipeModelList.get(position).getMainpic());
//        System.out.println("urlink="+recipesShortModelList.get(position).getMainpicurl());
        holder.mainimage.setImageURI(uri);

    }
    public SingleViewModel onCreateViewHolder(ViewGroup viewGroup, int i){
        SingleViewModel singleViewModel;
        View v;
        v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recipe_list_adapter,viewGroup,false);
        singleViewModel=new SingleViewModel(v);
        return  singleViewModel;
    }
}
