package com.jhakkasrecipes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecipesModel {
    @SerializedName("recipeshort")
    RecipesShortModel recipesShortModel;

    @SerializedName("detail")
    String detail;

    @SerializedName("videolink")
    String videourl;

    @SerializedName("recipesparalist")
    private List<RecipeParagraph> recipeParagraphs;

    public List<RecipeParagraph> getRecipeParagraphs(){
        return this.recipeParagraphs;
    }

    public void setRecipeParagraphs(List<RecipeParagraph> recipeParagraphs){
        this.recipeParagraphs=recipeParagraphs;
    }


    public RecipesShortModel getRecipesShortModel() {
        return this.recipesShortModel;
    }

    public String getDetail(){
        return this.detail;
    }

    public String getVideourl(){
        return this.videourl;
    }

    public void setRecipesShortModel(RecipesShortModel recipesShortModel){
        this.recipesShortModel=recipesShortModel;
    }

    public void setDetail(String detail){
        this.detail=detail;
    }

    public void setVideourl(String vidurl){
        this.videourl=vidurl;
    }
}
