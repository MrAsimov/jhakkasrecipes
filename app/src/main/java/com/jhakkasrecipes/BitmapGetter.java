package com.jhakkasrecipes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Napo on 20/11/17.
 */

public class BitmapGetter extends AsyncTask<String,Void,Bitmap> {
    private final TaskListener taskListener;
    public BitmapGetter(TaskListener t){
        this.taskListener=t;
    }
    protected Bitmap doInBackground(String... params){
        try{
            String picurl=params[0];
            Log.d("","myTask1"+picurl);
            URL url = new URL(picurl);
            Log.d("","myTask2");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            Log.d("","myTask3");
            connection.setDoInput(true);
            Log.d("","myTask4");
            connection.connect();
            Log.d("","myTask5");
            InputStream input = connection.getInputStream();
            Log.d("","myTask6");
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.d("","myTask7");
            return myBitmap;
        }catch(Exception e){
            Log.d("","myTask1 Exception");
        }
        return null;
    }
    @Override
    protected  void onPostExecute(Bitmap result){
        super.onPostExecute(result);
        try {
            if(this.taskListener != null) {
                this.taskListener.onFinished(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface TaskListener {
        public void onFinished(Bitmap result);
    }
}
