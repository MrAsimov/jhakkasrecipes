package com.jhakkasrecipes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

/**
 * Created by Napo on 20/11/17.
 */

public class Share {
    private String LOG_TAG = "SHARE_log_tag";
    private String text, subject, picurl;
    private Activity activity;
    public Share(Activity mActivity){
        this.activity=mActivity;
    }
    public Share(Activity mActivity, String t) {
        this.text = t;
        this.activity = mActivity;
    }

    public Share(Activity mActivity, String t, String pic) {
        this.text = t;
        this.picurl = pic;
        this.activity = mActivity;
    }

    public Share(Activity mActivity, String t, String pic, String sub) {
        this.text = t;
        this.subject = sub;
        this.picurl = pic;
        this.activity = mActivity;
    }

    public void shareFunction() {
        if (picurl != null && !picurl.isEmpty() && picurl.length() > 0) {
            try {
                BitmapGetter bitmapGetter = new BitmapGetter(new BitmapGetter.TaskListener() {
                    @Override
                    public void onFinished(Bitmap m) {
                        shareTextPhoto(text, m);
                    }
                });
                bitmapGetter.execute(picurl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            shareText(text);
        }
    }

    private void shareTextPhoto(String text, Bitmap m) {
        //Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);//commented on 30th nov
        Intent intent = new Intent(Intent.ACTION_SEND);//added on 30th nov
        String bitmapPath = MediaStore.Images.Media.insertImage(activity.getContentResolver(), m, "title", null);
        Log.d(LOG_TAG,"shareTextPhoto bitmapPath="+bitmapPath);
        if(bitmapPath!=null)//it may be null when the user hasn't uploaded his image
        {
            try {
                Uri bitmapUri = Uri.parse(bitmapPath);
                intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, text);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(Intent.createChooser(intent, "Share story via..."));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            shareText(text);
        }
    }

    private void shareText(String text) {
        Log.d(LOG_TAG, "shareText inside");
        try {
            Log.d(LOG_TAG, "shareText 2");
            Intent intent = new Intent(Intent.ACTION_SEND);
            Log.d(LOG_TAG, "shareText 3");
            intent.putExtra(Intent.EXTRA_TEXT, text);
            Log.d(LOG_TAG, "shareText 6");
            intent.setType("text/plain");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.activity.startActivity(Intent.createChooser(intent, "Share image via..."));
        } catch (Exception e) {

        }
    }
    public void shareProduct(String productUrl){
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            sharingIntent.setType("text/html");
            sharingIntent.putExtra(Intent.EXTRA_TEXT,productUrl);
            sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.activity.startActivity(Intent.createChooser(sharingIntent, "Share using"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
