package com.jhakkasrecipes;
import com.google.gson.annotations.SerializedName;
public class RecipeParagraph {
    @SerializedName("paratype")
    private String paratype;

    public String getParatype(){
        return this.paratype;
    }
    public void setParatype(String paratype){
        this.paratype=paratype;
    }

    @SerializedName("parahtml")
    private String para_html;

    public void setPara_html(String para_html){
        this.para_html=para_html;
    }

    public String getPara_html(){
        return this.para_html;
    }

    @SerializedName("paratext")
    private String para_text;

    @SerializedName("paraimage")
    private String para_image;

    @SerializedName("paravideo")
    private String para_video;

    public void setPara_text(String text){
        this.para_text=text;
    }
    public void setPara_image(String para_image ){
        this.para_image=para_image;
    }

    public void setPara_video(String para_video){
        this.para_video=para_video;
    }
    public String getPara_text(){
        return this.para_text;
    }

    public String getPara_image(){
        return this.para_image;
    }

    public String getPara_video(){
        return this.para_video;
    }
}
